﻿using UI;
using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ExternalOutput.Parse;

namespace BlackBox
{
	public class PreloaderGui : MonoBehaviour
	{
		class PreloadedStructure {
			public string Label {
				get;
				set;
			}

			public string File {
				get;
				set;
			}

			public string Formula {
				get;
				set;
			}

			public string Format {
				get {
					return File.Split('.').Last().ToLower();
				}
			}

			public static PreloadedStructure From(string name, string file, string formula) {
				return new PreloadedStructure {
					Label = name,
					File = file,
					Formula = formula
				};
			}
		}

		static readonly Dictionary<string, PreloadedStructure[]> sPreloadedMolecules =
			new Dictionary<string, PreloadedStructure[]> {
			{"Moléculas:", new [] {
					PreloadedStructure.From("Trifluoreto de Bório (BF3)", "BF3.xyz", "Trifluoreto_de_boro"),
					PreloadedStructure.From("Etanol (C2H6O)", "C2H6O.pdb", "Etanol"),
					PreloadedStructure.From("Metano (CH4)", "CH4.pdb", "CH4"),
					PreloadedStructure.From("Dióxido de Carbono (CO2)", "CO2.xyz", "CO2"),
					PreloadedStructure.From("Água (H2O)", "H2O.xyz", "H2O"),
					PreloadedStructure.From("Sulfeto de Hidrogênio (H2S)", "H2S.xyz", "H2S"),
					PreloadedStructure.From("Ácido Clorídrico (HCl)", "HCl.xyz", "HCl"),
					PreloadedStructure.From("Cianeto de Hidrogênio (HCN)", "HCN.pdb", "HCN"),
					PreloadedStructure.From("Gás Nitrogênio (N2)", "N2.xyz", "N2"),
					PreloadedStructure.From("Amônia (NH3)", "NH3.pdb", "NH3"),
					PreloadedStructure.From("Gás Oxigênio (O2)", "O2.xyz", "Oxigénio"),
					PreloadedStructure.From("Quitosana", "chitosan.xyz", "Quitosana"),
				}},					
		};



		Vector2 mScrollPosition = Vector2.zero;
		public bool mShowingMenu;
		public bool ShowingMenu { // Luiz: must not be auto property, because I want it visible in the editor.
			get {
				return mShowingMenu;
			}
			set {
				mShowingMenu = value;
			}
		}
			
		public bool DrawMenuOption() {
			if(GUILayout.Button(new GUIContent("    MOLÉCULAS OFFLINE    ", "Menu de moléculas pré-carregadas")))
			{
				GUIDisplay.Instance.PDBError = false;
				GUIDisplay.Instance.XYZError = false;
				GUIMoleculeController.Instance.showOpenMenu = false;
				GUIMoleculeController.Instance.toggle_HELP = false;
				UIData.Instance.hasMoleculeDisplay = false;
				ShowingMenu = true;
			}

			return ShowingMenu;
		}

		void OnGUI() {
			if (!ShowingMenu)
				return;

			if (GUIMoleculeController.Instance.showOpenMenu)
				return;

			if (GUIMoleculeController.Instance.toggle_HELP)
				return;

			if (UIData.Instance.hasMoleculeDisplay)
				return;

			if ((GUIDisplay.Instance.PDBError) && (GUIDisplay.Instance.XYZError))
				return;

			var frame = Rectangles.openRect;

			Rectangles.SetFontSize();
			GUI.Window(
				1332,
				frame,
				id => {
					var innerFrame = frame;
					innerFrame.x = innerFrame.y = 0;
					var sizeRect = innerFrame;
					sizeRect.height *= 2;
					sizeRect.width -= 10;

					if(DrawWindowTitleBar()) {
						ShowingMenu = false;
						return;
					} 

					var buttonAligment = GUI.skin.button.alignment;
					GUI.skin.button.alignment = TextAnchor.MiddleCenter;

					foreach(var structureType in sPreloadedMolecules) {
						GUILayout.Label(string.Empty);
						GUILayout.Label(structureType.Key.ToUpper());

						foreach(var structure in structureType.Value) {
							if(GUILayout.Button(structure.Label)) {
								ShowingMenu = false;
								LoadStructure(structure);
								GUIMoleculeController.Instance.umolbase = "https://pt.wikipedia.org/wiki/" + structure.Formula;
								break;
							}
						}
					}

					GUI.skin.button.alignment = buttonAligment;
				},
				string.Empty
			);
		}

		void LoadStructure(PreloadedStructure structure) {
			GUIDisplay.Instance.Clear(false);
			GUIMoleculeController.Instance.showOpenMenu = false;

			string path = Path.Combine(
				Application.streamingAssetsPath,
				Path.Combine(
					"Structures",
					structure.File
				)
			);

			var format = structure.Format;
			if(format == "xyz" || format == "pdb") {
				GUIDisplay.Instance.OpenFileCallback(path);
			} 
		}

		static bool DrawWindowTitleBar() {
			GUILayout.BeginHorizontal();

			GUILayout.FlexibleSpace();
			GUILayout.Label("MOLÉCULAS OFFLINE");
			GUILayout.FlexibleSpace();

			bool close = GUILayout.Button("X");

			GUILayout.EndHorizontal();

			return close;
		}

		public static PreloaderGui FindFirstInstance() {
			return GameObject.Find("Preloader").GetComponent<PreloaderGui>();
		}
	}
}

